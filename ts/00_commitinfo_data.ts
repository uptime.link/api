/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@uptime.link/api',
  version: '2.0.0',
  description: 'the api package for uptime.link'
}
